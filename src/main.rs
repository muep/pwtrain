extern crate bcrypt;
extern crate getopts;
extern crate rpassword;

fn check(src: &str) {
    let hash = {
        use std::io::Read;

        let mut f = std::fs::File::open(src).unwrap();

        let mut buf = String::new();

        f.read_to_string(&mut buf).unwrap();

        buf
    };

    loop {
        let password = rpassword::prompt_password_stderr("Password: ").unwrap();

        if bcrypt::verify(&password, &hash).unwrap() {
            println!("ACK");
            return;
        } else {
            println!("NAK");
        }
    }
}

fn new(dest: &str) {
    use std::io::Write;

    let password = prompt_password();
    let hash = bcrypt::hash(&password, bcrypt::DEFAULT_COST).unwrap();

    let mut f = std::fs::File::create(dest).unwrap();
    f.write_all(hash.as_bytes()).unwrap();
}

fn prompt_password() -> String {
    loop {
        let fst = rpassword::prompt_password_stderr("Password: ").unwrap();
        let snd = rpassword::prompt_password_stderr("Retype: ").unwrap();

        if fst == snd {
            return fst;
        }

        println!("The passwords must be identical!");
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();

    let opts = {
        let mut opts = getopts::Options::new();

        opts.optflag("n", "new", "set up a new password file");
        opts.optflag("h", "help", "print this help and exit");
        opts.optflag("v", "version", "print application version");

        opts
    };

    let usage = || {
        let brief = format!("Usage: {} [-n] [-h] [-v] FILENAME", &args[0]);
        opts.usage(&brief)
    };

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(r) => {
            print!("{}\n{}", r.to_string(), usage());
            return;
        }
    };

    if matches.opt_present("h") {
        print!("{}", usage());
        return;
    }

    if matches.opt_present("v") {
        println!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
        return;
    }

    let file = match matches.free.first() {
        Some(f) => f,
        None => {
            print!("FILENAME is required.\n{}", usage());
            return;
        }
    };

    if matches.opt_present("n") {
        new(file);
    } else {
        check(file);
    }
}
