This is a simple tool to assist with memorizing a password before one
commits to changing it. No nifty mind tricks here. It is just
something where you can set a password and later try authenticating
yourself with that password.

# Example uses

First, store the password hash to a file:

    $ pwtrain -n myaccount
    Password: <type a new password here>
    Retype: <type the password again to verify>
    $

Now there should be a file named myaccount which can be used to try
authentication like this:

    $ pwtrain myaccount
    Password: <type some different password>
    NAK
    Password: <again some bad password>
    NAK
    Password: <if the right password is given, the tool exits>
    ACK
    $

# Build instructions
Install rustc and cargo. Then in the root of this directory, run this:

    cargo install
